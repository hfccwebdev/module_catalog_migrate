<?php

namespace Drupal\catalog_migrate\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\user\Plugin\migrate\source\d7\Role as D7Role;

/**
 * Override default permissions handling during role migration.
 *
 * @MigrateSource(
 *   id = "catalog_d7_user_role"
 * )
 */
class Role extends D7Role {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Call the parent first or it will undo our changes.
    $result = parent::prepareRow($row);
    $permissions = [];
    $row->setSourceProperty('permissions', $permissions);
    return $result;
  }

}
