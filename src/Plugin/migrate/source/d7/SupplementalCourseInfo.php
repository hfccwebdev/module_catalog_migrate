<?php

namespace Drupal\catalog_migrate\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as D7Node;

/**
 * Add some field content from source course_master content type.
 *
 * @MigrateSource(
 *   id = "catalog_d7_node_supplemental_course_info",
 *   source_provider = "node"
 * )
 */
class SupplementalCourseInfo extends D7Node {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Call the parent first or it will undo our changes.
    if (parent::prepareRow($row)) {

      // Manipulate data related to the course master.
      if ($value = $row->getSourceProperty('field_course_master')) {
        if (!empty($value)) {
          $course_master = reset($value)['target_id'];

          // Map 0/1 field values to no/yes
          $map = ['0' => 'no', '1' => 'yes'];


          // Migrate fields from source course_master content type.

          foreach (['field_crs_macrao', 'field_crs_mta', 'field_crs_gened_outcomes'] as $field_name) {
            $field_table = "field_data_$field_name";
            $column_name = $field_name . "_value";
            $result = $this->select($field_table, 'f')
              ->fields('f')
              ->condition('entity_type', 'node')
              ->condition('bundle', 'course_master')
              ->condition('entity_id', $course_master)
              ->orderBy('delta')
              ->execute()->fetchAll();
            $new = [];
            foreach ($result as $field_row) {
              $data = $field_row[$column_name];
              if (in_array($field_name, ['field_crs_macrao', 'field_crs_mta'])) {
                $data = isset($map[$data]) ? $map[$data] : $data;
              }
              $new[] = ['value' => $data];
            }
            if (!empty($new)) {
              $row->setSourceProperty($field_name, $new);
            }
          }

          // Migrate fields from source new_course_application content type.

          $join = "f.revision_id = m.revision_id AND m.entity_type = 'node' AND m.bundle = 'new_course_application' AND m.delta = 0";

          foreach ([
            'field_crs_transfer',
            'field_program_xfer_schools',
            'field_crs_transfer_notes',
            'field_crs_fee_desc',
            'field_crs_reimburse_code',
            'field_crs_student_audit',
          ] as $field_name) {
            $field_table = "field_data_$field_name";
            $column_name = $field_name . "_value";
            $query = $this->select($field_table, 'f')
              ->fields('f');
            $query->leftJoin('field_data_field_course_master', 'm', $join);
            $query->condition('m.field_course_master_target_id', $course_master);
            $query->orderBy('f.delta');
            $result = $query->execute()->fetchAll();
            if (!empty($result)) {
              $new = [];
              foreach ($result as $field_row) {
                $data = $field_row[$column_name];
                if (in_array($field_name, ['field_crs_transfer', 'field_crs_student_audit'])) {
                  $new[] = ['value' => isset($map[$data]) ? $map[$data] : $data];
                }
                elseif (in_array($field_name, ['field_crs_fee_desc', 'field_crs_transfer_notes'])) {
                  $new[] = ['value' => $data, 'format' => 'markdown'];
                }
                elseif ($field_name == 'field_program_xfer_schools') {
                  $new[] = ['target_id' => $data];
                }
                else {
                  $new[] = ['value' => $data];
                }
              }
              if (!empty($new)) {
                // echo "$field_name: " . json_encode($new) . PHP_EOL;
                $row->setSourceProperty($field_name, $new);
              }
            }
          }

          // Set field_crs_inst_outcome values based on subject and number from course_master.

          $course_name = [];
          $categories = [];
          foreach (['field_crs_subj', 'field_crs_num'] as $field_name) {
            $field_table = "field_data_$field_name";
            $column_name = $field_name . "_value";
            $result = $this->select($field_table, 'f')
              ->fields('f')
              ->condition('entity_type', 'node')
              ->condition('bundle', 'course_master')
              ->condition('entity_id', $course_master)
              ->orderBy('delta')
              ->execute()->fetchAll();
            if (!empty($result)) {
              $course_name[$field_name] = reset($result)[$column_name];
            }
          }
          if (!empty($course_name['field_crs_subj']) && !empty($course_name['field_crs_num'])) {
            $result = $this->select('hfc_institutional_outcomes', 'o')
              ->fields('o', ['category'])
              ->condition('subject', $course_name['field_crs_subj'])
              ->condition('number', $course_name['field_crs_num'])
              ->orderBy('category')
              ->execute()->fetchAll();
            if (!empty($result)) {
              foreach ($result as $field_row) {
                $categories[] = $field_row['category'];
              }
            }

            $subjects = [
              'ANTH' => 'cat4',
              'CRJ' => 'cat4',
              'GEOG' => 'cat4',
              'HIST' => 'cat4',
              'PHIL' => 'cat4',
              'POLS' => 'cat4',
              'PSY' => 'cat4',
              'SOC' => 'cat4',
              'WR' => 'cat4',
              'ARA' => 'cat5',
              'CHN' => 'cat5',
              'FRE' => 'cat5',
              'GER' => 'cat5',
              'ITAL' => 'cat5',
              'PHIL' => 'cat5',
              'SPN' => 'cat5',
              'WR' => 'cat5',
              'ASTR' => 'cat6a',
              'ATMS' => 'cat6a',
              'BIO' => 'cat6a',
              'CHEM' => 'cat6a',
              'ENGR' => 'cat6a',
              'GEOL' => 'cat6a',
              'PHYS' => 'cat6a',
              'PSCI' => 'cat6a',
              'SCI' => 'cat6a',
            ];

            if (!empty($subjects[$course_name['field_crs_subj']]) && is_numeric($course_name['field_crs_num']) && ($course_name['field_crs_num'] >= 100)) {
              if (!in_array($subjects[$course_name['field_crs_subj']], $categories)) {
                $categories[] = $subjects[$course_name['field_crs_subj']];
              }
            }

            if (!empty($categories)) {
              sort($categories, SORT_STRING);
              $value = [];
              foreach ($categories as $category) {
                $value[] = ['value' => $category];
              }
              $row->setSourceProperty('field_crs_inst_outcome', $value);
            }
          }
        }
      }

      // Convert field_crs_waitlist to no/yes
      if ($value = $row->getSourceProperty('field_crs_waitlist')) {
        if (!empty($value)) {
          $map = ['0' => 'no', '1' => 'yes'];
          foreach ($value as $delta => $item) {
            $value[$delta]['value'] = !empty($map[$item['value']]) ? $map[$item['value']] : $item['value'];
          }
          $row->setSourceProperty('field_crs_waitlist', $value);
        }
      }

      // Convert field_hank_courses_id to entity reference.
      if ($value = $row->getSourceProperty('field_hank_courses_id')) {
        if (!empty($value)) {
          foreach ($value as $delta => $item) {
            $value[$delta]['target_id'] = $item['value'];
          }
          $row->setSourceProperty('field_hank_courses_id', $value);
        }
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
